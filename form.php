<?php

class form {
	private $error = [];
	public function processForm() {
	  if ($this->dataReceived()) {
		if ($this->valid()) {
	      $this->process();
       }
     }
	 $error = $this->error;
     include('urlap.php');
   }
  protected function dataReceived() {
    return !empty($_POST);
  }
  protected function valid() {
	  if ($_POST['nev'] === "") {
        $this->error[] = 'A nevet meg kell adni!';
	  }
	  return empty($this->error);
  }
  protected function process() {
    print "Hello {$_POST['nev']}!";
	die();
  }
}